<?php
$dbh = new PDO('mysql:host=localhost;dbname=annotation', "homestead", "secret");
$sth = $dbh->prepare("SELECT claims from claims");
$sth->execute();

$result= $sth->fetchAll(PDO::FETCH_ASSOC);
$claims=array();
foreach($result as $element){
    foreach($element as $current_claim){
        array_push($claims,"Claim : ".$current_claim);
    }
}

$sth2 = $dbh->prepare("SELECT premise, claim from premise");
$sth2->execute();

$premise= $sth2->fetchAll(PDO::FETCH_ASSOC);

foreach($premise as $current){
    foreach ($result as $row){
        foreach ($row as $claim){
            if ($current['claim'] == $claim){
                array_push($claims,"\t <b> Premise : </b>" .$current['premise']);
                array_push($claims,"\t <b> Claim : </b> ".$claim);
            }else if($current['claim'] != $claim){
                array_push($claims,"Claim : ".$claim);
            }
        }
    }
}
echo json_encode(($claims));

?>