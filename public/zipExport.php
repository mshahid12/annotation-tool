<?php
$userid= $_GET['userid'];
$url= $_GET['url'];
$topic= $_GET['topic'];
$userid = trim($userid, "''");
//$url = trim($url, "''");
//$topic = trim($topic, "''");
$fileName = 'Export_all_.zip';
$zip = new ZipArchive;
$result_zip = $zip->open($fileName, ZipArchive::CREATE );    // We open the file
if ($result_zip === TRUE) {
    $pdo = new PDO('mysql:host=localhost;dbname=annotation', "homestead", "secret");
    $Qselect = $pdo->prepare("select id from docs where url = ?");
    $Qselect->execute(array($url));
    $rows = $Qselect->fetchAll(PDO::FETCH_ASSOC);
    $docid = implode($rows[0]);

    /*   $Qselect = $pdo->prepare("select docid from sentence where docid = ?");
       $Qselect->execute(array_map('strval',$docid));
       $rows = $Qselect->fetchAll(PDO::FETCH_ASSOC);
       $docid1 = $rows[0];*/

    $Qselect = $pdo->prepare("select @userId := :value as annotatorID, b.docid, b.id as sentenceId, b.sentences as sentenceText, a.id as claimId, a.claims as claimText from claims a, sentence b where b.docid = '$docid' AND a.sentence like b.sentences AND NOT a.sentence ='none'");
    $Qselect->execute(array(':value' => $userid));
    //$Qselect->execute(array(':value' => $docid));
    $rows = $Qselect->fetchAll(PDO::FETCH_ASSOC);
    $columnNames = array();
    if (!empty($rows)) {
        $firstRow = $rows[0];
        foreach ($firstRow as $colName => $val) {
            $columnNames[] = $colName;
        }
    }
    $fc = fopen('php://temp/maxmemory:1048576', 'w');
    fputcsv($fc, $columnNames);
    foreach ($rows as $row) {
        fputcsv($fc, $row);
    }
    //fputcsv($fc, $docid);
    rewind($fc);
    $file = 'claim';
    $zip->addFromString(''.$file.'.csv', stream_get_contents($fc) );
    fclose($fp);

    $Qselect = $pdo->prepare("select @userId := :value as annotatorID,d.docid, d.id as SentenceId, d.sentences as sentenceText, c.id as premiseId, c.premise as premiseText from premise c, sentence d where d.docid = '$docid' AND d.sentences LIKE CONCAT ('%', c.premise, '%' )");
    $Qselect->execute(array(':value' => $userid));
    $rows = $Qselect->fetchAll(PDO::FETCH_ASSOC);
    $columnNames = array();
    if (!empty($rows)) {
        $firstRow = $rows[0];
        foreach ($firstRow as $colName => $val) {
            $columnNames[] = $colName;
        }
    }
    $fp = fopen('php://temp/maxmemory:1048576', 'w');
    fputcsv($fp, $columnNames);
    foreach ($rows as $row) {
        fputcsv($fp, $row);
    }
    rewind($fp);
    $file = 'Premise';
    $zip->addFromString(''.$file.'.csv', stream_get_contents($fp) );
    fclose($fp);

    $Qselect = $pdo->prepare("SELECT @userId := :value as annotatorID,A.docid, A.claimsentenceId, A.claimId, A.claimText, B.premiseSentenceId, B.premiseId, B.premiseText FROM (SELECT @row_number1:=@row_number1+1 AS RowNumber1,b.docid, b.id as claimsentenceId, a.id as claimId, a.claims as claimText FROM claims a, sentence b, (SELECT @row_number1:=0)AS x WHERE b.docid = '$docid' AND a.sentence like b.sentences ORDER BY b.docid) AS A RIGHT JOIN (SELECT @row_number2:=@row_number2+1 AS RowNumber2,d.docid, d.id as premiseSentenceId, c.id as premiseId, c.premise as premiseText, c.claim as claimText FROM premise c, sentence d, (SELECT @row_number2:=0) AS y WHERE d.docid = '$docid' AND d.sentences LIKE CONCAT ('%', c.premise,'%') ORDER BY d.docid) AS B ON A.RowNumber1=B.RowNumber2");
    $Qselect->execute(array(':value' => $userid));
    $rows = $Qselect->fetchAll(PDO::FETCH_ASSOC);
    $columnNames = array();
    if (!empty($rows)) {
        $firstRow = $rows[0];
        foreach ($firstRow as $colName => $val) {
            $columnNames[] = $colName;
        }
    }
    $fp = fopen('php://temp/maxmemory:1048576', 'w');
    fputcsv($fp, $columnNames);
    foreach ($rows as $row) {
        fputcsv($fp, $row);
    }
    rewind($fp);
    $file = 'Premise_Claim_Linked';
    $zip->addFromString(''.$file.'.csv', stream_get_contents($fp) );

    $zip->close();
    ob_clean();
    ob_end_flush();
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename='.$fileName);
    readfile($fileName);
    unlink($fileName);
    exit();
}
else {
    echo 'Failed, code:' . $result_zip;
}