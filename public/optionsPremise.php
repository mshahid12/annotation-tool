<?php

$dbh = new PDO('mysql:host=localhost;dbname=annotation', "homestead", "secret");
$sth = $dbh->prepare("SELECT premise from premise");
$sth->execute();

$result= $sth->fetchAll(PDO::FETCH_ASSOC);

$premises=array();
foreach ($result as $row){
    foreach ($row as $premise)
        array_push($premises,$premise);
}

echo json_encode($premises);

?>