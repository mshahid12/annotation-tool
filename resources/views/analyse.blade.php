@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Attack Or Support</div>
                    <div class="panel-body">
                        <table>
                            <tr>
                                <th>Claim -> Premise</th>
                                <th>Attack Support None</th>
                            </tr>
                            <tr>
                                <td>Alfreds Futterkiste</td>
                                <td>Germany</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>

    <script>

    </script>



@endsection