@extends('layouts.app')

@section('content')
    <div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <form>
                    <div class="form-group">
                        <label for="url">Enter Url</label>
                        <input type="text" class="form-control" id="agent" placeholder="Enter url">
                        <br>
                        <label for="url">Enter Topic</label>

                        <input type="text" class="form-control" id="topic_agent" placeholder="Enter Topic">


                        <small id="emailHelp" class="form-text text-muted">We'll never share your url with anyone
                            else.
                        </small>
                    </div>

                    <button type="submit" class="btn btn-primary" onclick="return urllink()">Get Content</button>
                    <button type="button" onclick="resultsExport(); " class="btn btn-primary">Export</button>
                    {{--<button type="submit" class="btn btn-primary" onclick="resultExport.php">Export</button>--}}
                    {{--<a href="resultExport.php">Export To Excel</a>--}}



                    <br><br>
                    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" onclick="progress_update()">
                        My Progress
                    </a>

                    <div class="collapse" id="collapseExample">
                        <div id="claim">

                        </div>
                    </div>




                </form>
                <br><br>
                <div class="panel panel-default">
                    <div class="animated fadeIn panel-heading">Web Contents</div>
                    <div class="panel-body">
                        <p id="msg"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .claimselection {
            background-color: #74b574;
        }

        .premiseselection {
            background-color: #F89406;
        }

        .claimdeletion {
            background-color: transparent;
        }

        .premisedeletion {
            background-color: transparent;
        }

        pre {
            white-space: pre-wrap;       /* Since CSS 2.1 */
            white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
            white-space: -pre-wrap;      /* Opera 4-6 */
            white-space: -o-pre-wrap;    /* Opera 7 */
            word-wrap: break-word;       /* Internet Explorer 5.5+ */
        }

    </style>

    <script>
        //vex.dialog.buttons.YES.text = 'Claim';
        //vex.dialog.buttons.NO.text = 'Premise';




        window.onbeforeunload = function () {
            return "Are you sure you want to navigate away from Annotation Tool?";
        };
        $(document).ready(function () {
            document.oncontextmenu = function () {
                return false;
            };

            $(document).mousedown(function (e) {
                if (e.button == 2) {
                    let claimlist = "";
                    $.ajax({
                        type: "post",
                        datatype: "json",
                        url: "optionsClaims.php",
                        success: function (msg) {
                            let test = '';
                            test = eval(msg);
                            for (let i = 0; i < test.length; i++) {
                                claimlist += "<option value=" + test[i] + ">" + test[i] + "</option>,";
                            }
                            claimlist = claimlist.substring(0, claimlist.length - 1);
                        }
                    });

                    let text = window.getSelection().toString();
                    if (text.length > 1) {
                        $('#check').append(text);
                        //alert(text);

                        todayDateString = new Date().toJSON().slice(0, 10);
                        vex.dialog.open({
                            message: 'Annotate your selected content : ' + text,
                            buttons: [
                                $.extend({}, vex.dialog.buttons.YES, {text: 'Claim'}),
                                $.extend({}, vex.dialog.buttons.NO, {text: 'Premise'}),
                                $.extend({}, vex.dialog.buttons.NO, {
                                    text: 'Cancel', click: function (e) {
                                        this.value = 'cancel';
                                        this.close();
                                    }
                                }),
                            ],
                            callback: function (value) {
                                if (value == true) {
                                    console.log("Claim selected");
                                    toastr.options = {
                                        "closeButton": false,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": false,
                                        "positionClass": "toast-top-right",
                                        "preventDuplicates": false,
                                        "onclick": null,
                                        "showDuration": "300",
                                        "hideDuration": "1000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    };
                                    toastr["success"](text, "Claim Added");

                                    $.ajax({
                                        type: "get",
                                        url: "claim.php?claim=" + text,
                                        success: function (html) {
                                        }
                                    });

                                    let range = window.getSelection().getRangeAt(0);
                                    if (text.length > 1) {

                                        let newNode = document.createElement("span");
                                        newNode.setAttribute("class", "claimselection");
                                        range.surroundContents(newNode);
                                        $(newNode).attr('unselectable', 'on')
                                            .css({
                                                '-moz-user-select': '-moz-none',
                                                '-moz-user-select': 'none',
                                                '-o-user-select': 'none',
                                                '-khtml-user-select': 'none',  //!* you could also put this in a class *!/
                                                '-webkit-user-select': 'none', //!* and add the CSS class here instead *!/
                                                '-ms-user-select': 'none',
                                                'user-select': 'none'
                                            }).bind('selectstart', function () {
                                        });
                                        $(newNode).click(function (e) {
                                            //alert("Do you want to delete this cliam ? " + text);
                                            vex.dialog.open({
                                                message: 'Do you want to delete this claim? ' + text,
                                                buttons: [
                                                    $.extend({}, vex.dialog.buttons.YES, {text: 'Yes'}),
                                                    $.extend({}, vex.dialog.buttons.NO, {text: 'No'})
                                                ],
                                                callback: function (value) {
                                                    if (value == true) {
                                                        toastr["error"](text, "Claim Deleted");
                                                        newNode.setAttribute("class", "claimdeletion");
                                                        $.ajax({
                                                            type: "get",
                                                            url: "deleteclaim.php?claim=" + text,
                                                            success: function (html) {
                                                            }

                                                        });
                                                    }
                                                }
                                            });
                                        });
                                    }
                                }
                                else if (value == false) {
                                    console.log("Premise selected");
                                    setInterval(refreshPartial, 500);
                                    function refreshPartial() {
                                        $.ajax({
                                            url: "optionsClaims.php"
                                        })
                                    }

                                    //here after premise selected
                                    //toastr["warning"](text, "Premise Added" );

                                    vex.dialog.open({
                                        message: 'Select your claim for your premise:',
                                        /*   input: [
                                         '<select id="claims_list">',


                                         '</select>',
                                         ].join(''), */

                                        input: [
                                            '<form>', '<fieldset>', '<lable>',
                                            "Claims:",
                                            '<select id="claims_list">',
                                            claimlist,
                                            '</select>'
                                        ].join(''),

                                        buttons: [
                                            $.extend({}, vex.dialog.buttons.YES, {text: 'Link'}),
                                            $.extend({}, vex.dialog.buttons.NO, {text: 'Cancel'})
                                        ],
                                        callback: function (data) {
                                            if (!data) {
                                                console.log('Cancelled')
                                            } else {
                                                let e = document.getElementById("claims_list");
                                                let claim_link = e.options[e.selectedIndex].text;
                                                console.log(claim_link);
                                                toastr["warning"](text, "Premise Added");
                                                toastr["success"](claim_link, "Premise Linked To Claim");
                                                $.ajax({
                                                    type: "get",
                                                    url: "premise.php?premise=" + text + "&" + "claim_link=" + claim_link,
                                                    success: function (html) {
                                                        //console.log(html(html));
                                                    }
                                                });
                                            }
                                        }
                                    });


                                    let range = window.getSelection().getRangeAt(0);
                                    if (text.length > 1) {

                                        let newNode = document.createElement("span");
                                        newNode.setAttribute("class", "premiseselection");
                                        range.surroundContents(newNode);
                                        $(newNode).attr('unselectable', 'on')
                                            .css({
                                                '-moz-user-select': '-moz-none',
                                                '-moz-user-select': 'none',
                                                '-o-user-select': 'none',
                                                '-khtml-user-select': 'none', /* you could also put this in a class */
                                                '-webkit-user-select': 'none', /* and add the CSS class here instead */
                                                '-ms-user-select': 'none',
                                                'user-select': 'none'
                                            }).bind('selectstart', function () {

                                        });
                                        $(newNode).click(function (e) {
                                            //alert("Do you want to delete this cliam ? " + text);
                                            vex.dialog.open({
                                                message: 'Do you want to delete this premise? ' + text,
                                                buttons: [
                                                    $.extend({}, vex.dialog.buttons.YES, {text: 'Yes'}),
                                                    $.extend({}, vex.dialog.buttons.NO, {text: 'No'})
                                                ],
                                                callback: function (value) {
                                                    if (value == true) {
                                                        toastr["error"](text, "Premise Deleted");
                                                        newNode.setAttribute("class", "premisedeletion");
                                                        $.ajax({
                                                            type: "get",
                                                            url: "deletepremise.php?premise=" + text,
                                                            success: function (html) {
                                                            }

                                                        });
                                                    }
                                                }
                                            });
                                        });
                                    }
                                }
                            }
                        })
                        return false;
                    }
                    return true;
                }
            });
        });
        function urllink() {
            let url_user = document.getElementById('agent').value;
            let topicvalue = document.getElementById('topic_agent').value;

            let dataStringtopic = topicvalue;
            let dataString = url_user;
            $.ajax({
                type: "get",
                url: "extract.php?url=" + url_user + "&" + "topic=" + topicvalue,

                data: dataString,
                cache: false,
                success: function (html) {
                    $("#msg").html(html);
                }
            });
            return false;
        }

        function reset() {
            console.log("reset pressed");
            return false;
        }

        function resultsExport() {
            console.log("Export pressed 1");
            console.log("userId:"+'{{Auth::user()->id}}');
            let url_user = document.getElementById('agent').value;
            let topicvalue = document.getElementById('topic_agent').value;
            console.log("docUrl:"+url_user);
            console.log("docTopicvalue:"+topicvalue);
            console.log("zipExport.php?userid='{{Auth::user()->id}}'" + "&url=" + url_user + "&topic=" + topicvalue);

            //window.open("zipExport.php?userid='{{Auth::user()->id}}'&url="+url_user+"&topic= 'topic_value' ", '_blank');
            window.open("zipExport.php?userid='{{Auth::user()->id}}'" + "&url=" + url_user + "&topic=" + topicvalue, '_blank');
            return false;
        }


        function progress_update(){
            //let url_user = document.getElementById('agent').value;
            //let dataString = url_user;
            $.ajax({
                type: "get",
                datatype: "json",
                url: "optionClaims2.php",
                success: function (msg) {
                    let test = '';
                    test = eval(msg);
                    let claimlist = "";
                    for (let i = 0; i < test.length; i++) {
                        claimlist +=test[i] + "\n"+"\n";
                    }
                    claimlist = claimlist.substring(0, claimlist.length);
                    document.getElementById('claim').innerHTML = "<pre>" + claimlist + "</pre>";
                }
            });
        }

    </script>
@endsection