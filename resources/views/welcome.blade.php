@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    This application is designed for annotation of links of any kind. So every user can maintain a database
                    of claims and premise.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
